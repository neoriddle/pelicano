package com.cacaolabs.lacomer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.cacaolabs.lacomer.activities.LoginActivity;
import com.cacaolabs.lacomer.activities.SettingsActivity;
import com.cacaolabs.lacomer.fragments.CategoriasF;
import com.cacaolabs.lacomer.fragments.MyFragmentActivity;
import com.cacaolabs.lacomer.fragments.PromocionesF;
import com.cacaolabs.lacomer.recursos.Recursos;
import com.google.android.gcm.GCMRegistrar;

public class MainActivity extends MyFragmentActivity {

    AsyncTask<Void, Void, Void> mRegisterTask;

    Context context;

    private static final int fragment_id = R.id.fragment_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // setTheme(R.style.Theme_Sherlock_Light_DarkActionBar);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        super.setFragmentId(fragment_id);
        setContentView(R.layout.main);

        // We take the support actionbar
        ActionBar ab = getSupportActionBar();

        // Titulo oculto
        //ab.setDisplayShowTitleEnabled(false);
        // Progress oculto
        setSupportProgressBarIndeterminateVisibility(false);

        ab.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        context = getSupportActionBar().getThemedContext();

        // we create the tabs
        ActionBar.Tab homeTab = ab.newTab().setText("Pasillos");
        ActionBar.Tab tagsTab = ab.newTab().setText("Favoritos");

        // We create the fragments
        Fragment homeFragment = new CategoriasF();
        Fragment tagsFragment = new PromocionesF(null);

        // And we set the tabsListener;
        homeTab.setTabListener(new MyTabsListener(homeFragment, this
                .getApplicationContext()));
        tagsTab.setTabListener(new MyTabsListener(tagsFragment, this
                .getApplicationContext()));

        ab.addTab(homeTab);
        ab.addTab(tagsTab);

        // ArrayAdapter<CharSequence> listAdapter =
        // ArrayAdapter.createFromResource(context, R.array.sections,
        // R.layout.sherlock_spinner_item);
        // ab.setListNavigationCallbacks(listAdapter, null);
        // listAdapter.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);

        final String regId = GCMRegistrar.getRegistrationId(this);
        if (regId.equals("")) {
            // Automatically registers application on startup.
            GCMRegistrar.register(this, Recursos.SENDER_ID);
        } else {
            // Device is already registered on GCM, check server.
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                // Skips registration.
                // mDisplay.append(getString(R.string.already_registered) +
                // "\n");
            } else {
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                final Context context = this;
                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // boolean registered =ServerUtilities.register(context,
                        // regId);
                        Log.i(Recursos.TAG, "Device registered: regId = "
                                + regId);
                        // At this point all attempts to register with the app
                        // server failed, so we need to unregister the device
                        // from GCM - the app will try to register again when
                        // it is restarted. Note that GCM will send an
                        // unregistered callback upon completion, but
                        // GCMIntentService.onUnregistered() will ignore it.
                        // if (!registered) {
                        // GCMRegistrar.unregister(context);
                        // }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_settings:
            startActivity(new Intent(this, SettingsActivity.class));
            break;
        default:
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickMenuSettings(View view) {
        startActivity(new Intent(this, SettingsActivity.class));
    }

    class MyTabsListener implements ActionBar.TabListener {
        public Fragment fragment;
        private final Context appContext;

        public MyTabsListener(Fragment fragment, Context ctx) {
            this.fragment = fragment;
            this.appContext = ctx;
        }

        @Override
        public void onTabReselected(Tab tab, FragmentTransaction ft) {
            // Toast.makeText(this.appContext, "Reselected!",
            // Toast.LENGTH_LONG).show();
        }

        @Override
        public void onTabSelected(Tab tab, FragmentTransaction ft) {
            ft.replace(R.id.fragment_container, fragment);
        }

        @Override
        public void onTabUnselected(Tab tab, FragmentTransaction ft) {
            ft.remove(fragment);
        }
    }

    public void onClickBtnMoreInfo(View view) {
        startActivity(new Intent(context, LoginActivity.class));
    }

}
