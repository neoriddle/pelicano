package com.cacaolabs.lacomer.pojos;

import java.io.Serializable;
import java.util.Date;

public class Promocion implements Serializable{

	private static final long serialVersionUID = -7194623771107305992L;


	public String pasillo_id;
	public Date fecha_ini;
	public Date fecha_fin;
	public Integer tipo;
	public String valor;
	public String html;

}
