package com.cacaolabs.lacomer.fragments;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.cacaolabs.lacomer.R;
import com.cacaolabs.lacomer.exceptions.OfflineException;
import com.cacaolabs.lacomer.interfaces.Update;
import com.cacaolabs.lacomer.pojos.Pasillo;
import com.cacaolabs.lacomer.recursos.Recursos;
import com.cacaolabs.lacomer.recursos.Recursos.AnsType;
import com.cacaolabs.net.Net;
import com.cacaolabs.ui.UI;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

@SuppressLint("ValidFragment")
public class CategoriasF  extends SherlockListFragment implements Update{
	int mNum;
	//	private Sonido sonido;
	//	private final int  audio = R.raw.tab;
	private CategoriesAdapter t_adapter;

	private String name	= null;
	private boolean resume=false;
	private Activity context;
	private ProgressDialog m_ProgressDialog = null;
	private View viewer = null;
	private DisplayImageOptions options;
	private ImageLoader imageLoader = ImageLoader.getInstance();





	@Override
	public void onCreate(Bundle savedInstanceState) {


		super.onCreate(savedInstanceState);


		setRetainInstance(true);
		this.context =getActivity();
		//		sonido = new Sonido(this.context);
		//		sonido.initSonido(this.audio);
		options = new DisplayImageOptions.Builder()
		.showStubImage(R.drawable.stub)
		.cacheInMemory()
		.cacheOnDisc()
		.build();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		this.viewer = inflater.inflate(R.layout.productos, container, false);

		if (getListAdapter() == null){
			this.t_adapter = new CategoriesAdapter(this.context, R.layout.category_item, new ArrayList<Pasillo>());
			setListAdapter(this.t_adapter);
		}
		//		try {
		//			((Productos) this.context).showHeader();
		//		} catch (Exception e) {}
		updateData();
		return this.viewer;
	}

	@Override
	public void onResume() {
		super.onResume();
		this.resume =true;
	}

	@Override
	public void onPause() {
		super.onPause();
		this.resume=false;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if(this.name!=null)
			((MyFragmentActivity)this.context).removeDir("/"+this.name);
		imageLoader.stop();
	}


	private class CategoriesAdapter extends ArrayAdapter<Pasillo> {
		private int textViewResourceId; 
		public CategoriesAdapter(Context context, int textViewResourceId, ArrayList<Pasillo> categories ) {
			super(context , textViewResourceId, categories);
			this.textViewResourceId = textViewResourceId;
		}
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			if (v == null) {
				LayoutInflater vi = LayoutInflater.from(CategoriasF.this.context);
				v = vi.inflate(this.textViewResourceId, null);
			}
			Pasillo o = super.getItem(position);
			if (o != null){				
				((TextView) v.findViewById(R.id.cat_title)).setText(o.nombre);
				((TextView) v.findViewById(R.id.cat_number)).setText("(" + String.valueOf(o.total) + ")");
				if (o.imagen_url.equals("")== false){
					ImageView image=(ImageView)v.findViewById(R.id.product_image);
					image.setVisibility(View.VISIBLE);
					if(CategoriasF.this.resume==true){
						imageLoader.displayImage(o.imagen_url, image, options);
					}
				}
				else{
					ImageView image=(ImageView)v.findViewById(R.id.product_image);
					RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT);
					image.setLayoutParams(params);
				}
				v.setBackgroundResource(R.drawable.selector_shape_fondo_list_item);
			}
			return v;
		}
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		//TODO
		Pasillo o = (Pasillo) getListAdapter().getItem(position);
		if (o != null && o.total >0){

			Fragment fragment = new PromocionesF(o.id) ;

			//			clear();
			//			//sonido.play(false);
			//			Fragment fragmnet = (o.final_ == false)   ? new CategoriasF(o.id) : new ProductosF(o.id);
			//			if(o.final_==false){
			//				((CategoriasF)fragmnet).setName(o.name);
			//			}
			//			else{
			//				((ProductosF)fragmnet).setName(o.name);
			//			}
			//			( (MyFragmentActivity) this.context).addDir(o.name);
			( (MyFragmentActivity) this.context).startNextFragment(fragment);
		}
		else {
			UI. showToast("Por el momento no hay promociones disponibles", getActivity());
		}
		
	}

	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = this.context.getMenuInflater();
		inflater.inflate(R.menu.menu_add_favoritos, menu);
		menu.setHeaderTitle(getString(R.string.opciones));

	}
	
	
	
	
	@Override
	public void updateData(){
		new Thread(null, this.runGetCategories, "getCategories").start();
		this.m_ProgressDialog = ProgressDialog.show(getActivity(), getString(R.string.espere), getString(R.string.obteniendo), true,true,
				new DialogInterface.OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				( (MyFragmentActivity) CategoriasF.this.context).removeFragment();
			}
		});
		clear();
	}

	public void setName(String name){
		this.name=new String(name);
	}

	private void clear(){
		this.t_adapter.clear();
		this.t_adapter.notifyDataSetChanged();
	}

	/*
	 *Runnables para ejecutar los webServices getCategories y getProducts 
	 *
	 */
	private Runnable runGetCategories = new Runnable(){
		@SuppressWarnings("unchecked")
		@Override
		public void run() {
			AnsType ansType = AnsType.UNKNOW;
			List <Pasillo> pasillos = null;
			try {
				String url;

				url = Recursos.URL_SERVER + "ls_promo_pasillo.php/" ;



				Type fooType = new TypeToken<List <Pasillo>>() {}.getType();

				pasillos = (List<Pasillo>) Net.sendDataAndGetObjectCache(url, fooType, null);
				ansType = AnsType.OK;
			}  catch (IOException e) {
				ansType = AnsType.NETWORK_PROBLEM;
				e.printStackTrace();
			} catch (OfflineException e) {
				ansType = AnsType.OFFLINE;
				e.printStackTrace();
			} catch (Exception e) {
				ansType = AnsType.UNKNOW;
			}
			CategoriasF.this.context.runOnUiThread(new ResGetCategories(pasillos,ansType));
		}
	};

	class  ResGetCategories implements Runnable {

		List <Pasillo> pasillos;
		AnsType ansType;
		public ResGetCategories (List <Pasillo> pasillos, AnsType ansType){
			if ( pasillos != null){
				this.pasillos = pasillos; 

			} 
			this.ansType=ansType;
		}
		@Override
		public void run() {
			CategoriasF.this.m_ProgressDialog.dismiss();
			clear();
			switch (this.ansType) {
			case OK:
				if(this.pasillos != null && this.pasillos.size() > 0){			
					for( Pasillo pasillo : this.pasillos)
						CategoriasF.this.t_adapter.add(pasillo);
				}
				//				else if(this.error != null){
				//					if (CategoriasF.this.context instanceof Productos)
				//						((Productos) CategoriasF.this.context).decreCount();
				//					( (MyFragmentActivity) CategoriasF.this.context).removeFragment();
				//					UI.showAlertDialog(this.error.title, this.error.message, getString(R.string.ok), CategoriasF.this.context, null).show();
				//				}
				CategoriasF.this.t_adapter.notifyDataSetChanged();
				break;
			case OFFLINE:
				if(CategoriasF.this.resume==true)
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_sin_internet), getString(R.string.ok), CategoriasF.this.context, null).create().show();
				( (MyFragmentActivity) CategoriasF.this.context).removeFragment();
				break;
			case NETWORK_PROBLEM:
				if(CategoriasF.this.resume==true)
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_error), getString(R.string.ok), CategoriasF.this.context, null).create().show();
				( (MyFragmentActivity) CategoriasF.this.context).removeFragment();
				break;
			case UNKNOW:
			case DATA_PROBLEM:
			default:
				if(CategoriasF.this.resume==true)
					UI.showAlertDialog(getString(R.string.error), getString(R.string.monedero_datos_corruptos), getString(R.string.ok), CategoriasF.this.context, null).create().show();
				( (MyFragmentActivity) CategoriasF.this.context).removeFragment();
				break;
			}
		}
	}


}
