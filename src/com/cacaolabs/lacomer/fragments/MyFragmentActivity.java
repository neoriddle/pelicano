package com.cacaolabs.lacomer.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.facebook.android.Facebook;


public abstract class MyFragmentActivity extends SherlockFragmentActivity {

	private int fagmentId = -1; 
	private String dir=null;
	private boolean remove=false,onResume=false;
	private int FB_CB = 10000;
	protected static final String APP_ID = "290602114343982";
	protected static final String[] PERMISSIONS = new String[] {"email", "user_birthday",  "read_stream",  "publish_stream"};
	private Facebook facebook = new Facebook(APP_ID);
	private String messageToShare="www.pagatodo.com";
	private String productToShare=null;
	//private DialogTwitterAuth mDialogTwitterAuth;
	//private Preferencias prefs;
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		//this.prefs= AppMarti.getInstance().getPrefs();
	}
	

	@Override
	protected void onResume() {
		if(remove==true){
			removeFragment();
			remove=false;
		}
		onResume=true;
		super.onResume();
	}


	@Override
	protected void onPause() {
		super.onPause();
		onResume=false;
		hideKeyboard();
	}

	//public abstract boolean MyOnOptionsItemSelected(MenuItem item);

	public void startNextFragment(Fragment fragment){
		FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(this.fagmentId, fragment);
		fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commitAllowingStateLoss();
		return;
	}

	public void removeFragment (){
		if(onResume==true)
			getSupportFragmentManager().popBackStack();
		else
			remove=true;
		return;
	}

	public Fragment getCurrentFragment_ (){
		return getSupportFragmentManager().findFragmentById(this.fagmentId);
	}

	/*
	 * Rastreo de P�ginas y Eventos
	 * 
	 * */
	public void addDir(String dir){
		this.dir=new String(this.dir.concat("/"+dir));
		//trackPages();
	}
	public void removeDir(String dir){
		try {

			if (this.dir.indexOf(dir)>= 0)
				this.dir=this.dir.substring(0,this.dir.length()-dir.length());
			//trackPages();

		} catch (Exception e) {}
	}



	public void trackEvents( String category , String action , String label, int value){
//		Intent intent = new Intent(Recursos.TRACK_EVENT);
//		intent.putExtra("CATEGORY", category);
//		intent.putExtra("ACTION", action);
//		intent.putExtra("LABEL", label);
//		intent.putExtra("VALUE", value);
//		sendBroadcast(intent);
	}

	
	
	public void hideKeyboard (){
		//sendBroadcast(new Intent(Recursos.HIDE_KEYBOARD));
	}

	/*
	 * GETTERS y SETTERS
	 * 
	 * */
	public void setDir(String dir){
		this.dir=new String(dir);
	}
	public String getDir(){
		return this.dir;
	}
	public int getFagmentId() {
		return this.fagmentId;
	}

	public void setFragmentId(int fragmenfagmentId) {
		this.fagmentId = fragmenfagmentId;
	}


	/*
	 * Twiter y Facebook
	 * 
	 * */
	@Override
	protected void onActivityResult(int rq, int res,Intent data) {
		super.onActivityResult(rq, res, data);
		if (rq == FB_CB)
			this.facebook.authorizeCallback(rq, res, data);
	}

	public void setMessageToShare(String message){
		this.messageToShare=message;
	}
	public void setProductToShare(String productName){
		this.productToShare=productName;
	}

	public void sharefb(){
//		String access_token = prefs.loadData("access_token");
//		String expiresString=prefs.loadData("access_expires");
//		long expires=0;
//		if(expiresString!=null)
//			expires = Long.valueOf( expiresString);
//
//		if(access_token != null) {
//			facebook.setAccessToken(access_token);
//		}
//		if(expires != 0) {
//			facebook.setAccessExpires(expires);
//		}
//
//		if(!facebook.isSessionValid()) {
//			facebook.authorize(MyFragmentActivity.this, PERMISSIONS, FB_CB,  new DialogListener(){
//				@Override
//				public void onComplete(Bundle values) {
//					prefs.saveData("access_token",  facebook.getAccessToken());
//					prefs.saveData("access_expires",  Long.valueOf( facebook.getAccessExpires() ).toString());
//
//					postFB ();
//				}
//				@Override
//				public void onFacebookError(FacebookError e) {}
//				@Override
//				public void onError(DialogError e) {}
//				@Override
//				public void onCancel() {}
//
//			});
//		}else {
//			postFB ();
//		}
//		
		
		return;
	}
	public void sharetw(){
//		if(prefs.loadData(TOKEN)!=null&&prefs.loadData(SECRET_TOKEN)!=null)
//			Tweet();
//		else{
//			mDialogTwitterAuth = new DialogTwitterAuth(this);
//			mDialogTwitterAuth.show();
//		}
		return;
	}
	public void Tweet(){
		//new AlertDialogShareTwitter(messageToShare, this).buildShow();
	}
	
	public void postFB (){

//		Bundle parameters = new Bundle();
//		parameters.putString("name", MyFragmentActivity.this.productToShare);
//		parameters.putString("link", MyFragmentActivity.this.messageToShare);
//		MyFragmentActivity.this.facebook.dialog(MyFragmentActivity.this, "feed", parameters, new DialogListener() {
//			@Override
//			public void onFacebookError(FacebookError e) {}
//
//			@Override
//			public void onError(DialogError e) {}
//
//			@Override
//			public void onComplete(Bundle values) {}
//
//			@Override
//			public void onCancel() {}
//		});
	}

}

